# Laboratorio CI/CD (Azurian)

## Requisitos  
- [Docker](https://www.docker.com/get-started/) instalado en equipo local  
- Servicios de docker en ejecuci&oacute;n  

## Iniciar contenedores  
Para iniciar los contenedores se requiere acceder mediante l&iacute;nea de comandos al directorio que corresponda según la arquitectura del ambiente local y posteriormente ejecutar el comando  
```
docker-compose up
```
*docker-compose* se encargar&aacute; de leer el archivo y ejecutar&aacute; las siguientes tareas:  
- Descargar im&aacute;genes de contenedores si no existen en ambiente local.  
- Crear vol&uacute;menes necesarios  
- Configurar una red virtual  
- Iniciar 2 contenedores (Jenkins y SonarQube) dentro de la red virtual, lo que facilitar&aacute; su posterior integraci&oacute;n  

Una vez finalizado el proceso, será posible acceder en el navegador web a http://localhost:8080 y a http://localhost:9000 para acceder a Jenkins y SonarQube respectivamente.  

## Configuraci&oacute;n Jenkins  
### Activaci&oacute;n inicial  
Al acceder a http://localhost:8080, Jenkins presentar&aacute; el siguiente formulario, en el que solicita una key para activar el servicio.  

![alt text](img/jenkins-install/02-jenkins_key_web.png "Jenkins password (Web)")

Durante el aranque del contenedor Jenkins, en el LOG se generar&aacute; la key de inicializaci&oacute;n que debemos buscar y copiar para ingresar en el formulario mencionado anteriormente y dar clic en *Continue*.  

![alt text](img/jenkins-install/01-jenkins_key.png "Jenkins password (LOG)")

Ahora se nos ofrece configurar la instalaci&oacute;n de plugins. S&oacute;lo damos clic en instalar plugins recomendados.  

![alt text](img/jenkins-install/03-jenkins_plugin_01.png "Selecci&oacute;n plugins")

Se nos presentar&aacute; la siguiente p&aacute;gina en donde debemos esperar a que se descarguen e instalen los plugins.

![alt text](img/jenkins-install/03-jenkins_plugin_02.png "Instalaci&oacute;n plugins")

A continuaci&oacute;n se nos solicita configurar la cuenta de administraci&oacute;n.  

![alt text](img/jenkins-install/04-jenkins_admin_config_data.png "Configuraci&oacute;n cuenta admin")

S&oacute;lo debemos llenar el formulario y pasar a la siguiente pantalla mediante el bot&oacute;n *Save and Continue*  

Ahora se nos pide configurar la URL de Jenkins, podemos mantener el valor por defecto y dar clic en *Save and Continue*.  

![alt text](img/jenkins-install/05-jenkins_url_config.png "URL Jenkins")

Ahora se nos presenta la siguiente pantalla que nos indica que Jenkins se encuentra listo para funcionar, por lo que damos clic al bot&oacute;n *Start using Jenkins*  

![alt text](img/jenkins-install/06-jenkins_ready.png "Jenkins listo")

Finalmente se nos presentar&aacute; el home de Jenkins  

![alt text](img/jenkins-install/07-jenkins_home.png "Home Jenkins")

### Configuraci&oacute;n JDK  
Debemos acceder a "Administrar Jenkins > Global Tool Configuration".

![alt text](img/jenkins-config/01-jenkins-config.png "Tool Configuration")

Buscar la secci&oacute;n **JDK**, para luego dar clic en "A&ntilde;adir JDK"

![alt text](img/jenkins-config/02-jenkins-config.png "Tool Configuration")

Contenedor tiene ya instalado OpenJDK 11, por lo que para este laboratorio s&oacute;lo necesitamos ingresar un nombre, desactivar el checkbox *Instalar desde java.sun.com* e ingresar la ruta del JAVA_HOME que en este caso es 
```
/opt/java/openjdk
```
Dar clic en bot&oacute;n *Apply* para guardar configuraci&oacute;n  

### Configurac&oacute;n Maven  
En la misma p&aacute;gina, buscar la secci&oacute;n **Maven** y dar clic en *A&ntilde;adir Maven*, ingesar un nombre (Ej.: maven3) y mantener activo el checkbox *Instalar autom&aacute;ticamente*.

![alt text](img/jenkins-config/03-jenkins-config.png "Maven")

Dar clic en bot&oacute;n *Apply* para guardar la configuraci&oacute;n  

### Instalar plugins SonarQube  
Desde la administraci&oacute; de Jenkins, acceder a la administraci&oacute;n de plugins

![alt text](img/jenkins-config/04-jenkins-config.png "Administraci&oacute;n Plugins")

En el Plugin Manager, seleccionar la pesta&ntilde;a *Todos los plugins* e ingresar la palabra **sonar** en el buscador  

![alt text](img/jenkins-config/05-jenkins-config.png "Administraci&oacute;n Plugins")

Activar el checkbox a la izquierda de **SonarQube Scanner** y luego dar clic en el bot&oacute;n *Install without restart*  

![alt text](img/jenkins-config/06-jenkins-config.png "Instalaci&oacute;n SonarQube Scanner")

Esperar a que finalice la instalaci&oacute;n.  

### Configuraci&oacute;n instalaci&oacute;n SonarQube Scanner  
Volver a "Administrar Jenkins > Global Tool Configuration" y buscar la secci&oacute;n **SonarQube Scanner**. Dar clic en *A&ntilde;adir SonarQube Scanner*, ingesar un nombre, mantener activo el checkbox *Instalar autom&aacute;ticamente*.

![alt text](img/jenkins-config/07-jenkins-config.png "Configuraci&oacute;n SonarQube Scanner")

Mantener la versi&oacute;n propuesta en la secci&oacute;n *Install from Maven Central* y dar clic en *Save* para persistir la configuraci&oacute;n.  

## Configuraci&oacute;n SonarQube  
### Actualizaci&oacute;n password administrador  
Accedemos a http://localhost:9000 mediante un navegador web y accedemos con las credenciales de administraci&oacute;n.  

![alt text](img/sonarqube-config/sonar_01.png "Login SonarQube")

Por defecto, las credenciales de administrador son **admin**/**admin**, que por seguridad se nos solicitar&aacute; cambiar.  

![alt text](img/sonarqube-config/sonar_02.png "SonarQube Password")

Una vez actualizada la password del usuario admin, se nos presenta el home de SonarQube  

![alt text](img/sonarqube-config/sonar_03.png "SonarQube Home")  

### Configuraci&oacute;n webhook hacia Jenkins  
Se requiere configurar un Webhook para SonarQube notifique a Jenkins de la finalizaci&oacute;n de los an&aacute;lisis de c&oacute;digo.  

Para esto accedemos a la configuraci&oacute;n de webhooks, para apuntar a Jenkins. Para esto debemos acceder a "Administration > Configuration > Webhooks"  

![alt text](img/sonarqube-config/sonar_04.png "SonarQube webhooks menu")  

Damos clic en el bot&oacute;n *Create*  

![alt text](img/sonarqube-config/sonar_05.png "SonarQube webhooks create")  

Se nos presentar&aacute; un formulario en que debemos asignar un nombre y una URL apuntando a Jenkins  

![alt text](img/sonarqube-config/sonar_06.png "SonarQube webhooks form")  

Podemos utilizar el nombre **jenkins** como dominio de dicho servicio. Esto se debe a la configuraci&oacute;n de red virtual creada por *docker-compose*  

![alt text](img/sonarqube-config/sonar_07.png "SonarQube webhooks list")  

### Generaci&oacute;n access token  
Para poder ejecutar an&aacute;lisis desde Jenkins, se requiere un token que nos permita la creaci&oacute;n/actualizaci&oacute;n de proyectos en SonarQube.  

Para generar un token, debemos acceder a la cuenta del usuario administrador  

![alt text](img/sonarqube-config/sonar_08.png "SonarQube admin account")  

Y en la secci&oacute;n *Security* se encuentra el mantenedor de tokens, donde se debe ingresar un nombre y dar clic en bot&oacute;n *Generate*  

![alt text](img/sonarqube-config/sonar_09.png "SonarQube token crud")  

Esto genera el token que debemos respaldar, puesto que no habrá forma de recuperarlo una vez salgamos de la p&aacute;gina  

![alt text](img/sonarqube-config/sonar_10.png "SonarQube new token")  

### Configuraci&oacute;n SonarQube Scanner en Jenkins  
Accedemos a Jenkins, espec&iacute;ficamente a "Administrar Jenkins > Configurar el Sistema"  

![alt text](img/sonarqube-config/sonar_11.png "Jenkins system configuration")  

En la secci&oacute;n SonarQube servers, habilitar el checkbox *Environment variables*  

![alt text](img/sonarqube-config/sonar_12.png "Jenkins SonarQube environment variables")  

Luego dar clic al bot&oacute; *Add SonarQube* e ingresar un nombre, la URL del servicio SonarQube y dar clic al bot&oacute;n *Add* y a la opci&oacute;n *Jenkins*  

![alt text](img/sonarqube-config/sonar_13.png "Jenkins SonarQube server URL")  

Se levantar&aacute; una ventana modal en que podremos registrar el token generado en la cuenta del administrador de SonarQube  

![alt text](img/sonarqube-config/sonar_14.png "Jenkins credentials provider")  

En este formulario se deben ingresar los valores como se muestra en la siguiente captura y presionar el bot&oacute;n *Add*  

![alt text](img/sonarqube-config/sonar_15.png "Jenkins credentials provider form")  

Valores a ingresar:  
- Domain: Global  
- Kind: Secret text  
- Scope: Global  
- Secret: Token generado en SonarQube  
- ID: Un nombre para identificar el token en la administraci&oacute;n de credenciales (si se deja en blanco, Jenkins generar&aacute; un identificador)  
- Description: Una descripci&oacute;n (opcional)

Finalmente, seleccionar el token reci&eacute;n creado y dar clic al bot&oacute;n *Guardar*  

![alt text](img/sonarqube-config/sonar_16.png "SonarQube token selection")  

